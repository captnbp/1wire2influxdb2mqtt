#!/usr/bin/env python
from influxdb import InfluxDBClient
import os
import sys
import yaml
import time
import json
import paho.mqtt.client as mqtt

def on_connect(mqttc, obj, flags, rc):
    print("rc: " + str(rc))


def on_message(mqttc, obj, msg):
    print(msg.topic + " " + str(msg.qos) + " " + str(msg.payload))


def on_publish(mqttc, obj, mid):
    print("mid: " + str(mid))
    pass

def read_temp(device_file):
    f = open(device_file, 'r')
    lines = f.readlines()
    f.close()
    while lines[0].strip()[-3:] != 'YES':
        time.sleep(0.2)
        lines = read_temp_raw()
    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos+2:]
        temp_c = float(temp_string) / 1000.0
        return temp_c

def get_config():
    """ Reads the configuration file """
    try:
        base_dir = os.path.abspath(os.path.dirname(__file__))
        config_file = os.path.join(base_dir, "conf.yml")
        stream = open(config_file, 'r')
    except FileNotFoundError as err:
        print('Cannot find config file {} : {}'.format(config_file, err), file=sys.stderr)
        sys.exit(1)
    except IOError as err:
        print('Cannot read config file {} : {}'.format(config_file, err), file=sys.stderr)
        sys.exit(1)
    try:
        config = yaml.load(stream)
        stream.close()
    except yaml.YAMLError as err:
        if hasattr(err, 'problem_mark'):
            mark = err.problem_mark
            print("Error position in YAML configuration file {}: ({}:{})".format(config_file, mark.line + 1,
                                                                                 mark.column + 1))
            sys.exit(1)
        else:
            print(
                "Error in YAML configuration file {} : {}".format(
                    config_file, err), file=sys.stderr
            )
            sys.exit(1)
    return config



def main():
    config = get_config()
    client = InfluxDBClient(config['influxdb']['host'],
                            config['influxdb']['port'],
                            config['influxdb']['user'],
                            config['influxdb']['password'],
                            config['influxdb']['db']
                            )
    mqttc = mqtt.Client()

    mqttc.on_message = on_message
    mqttc.on_connect = on_connect
    mqttc.on_publish = on_publish

    mqttc.username_pw_set(config['mqtt']['username'], password=config['mqtt']['password'])
    mqttc.connect(config['mqtt']['host'], port=config['mqtt']['port'], keepalive=60)

    mqttc.loop_start()

    while True:
        data = []
        for sensor in config['sensors']:
            device_file = '/sys/bus/w1/devices' + sensor['id'] + '/w1_slave'
            try:
                temp = read_temp(device_file)
                measure = { "measurement": "temperature",
                            "tags": {
                                "region": config['region'],
                                "zone": sensor['zone']
                            },
                        "fields": {
                            "value": temp
                        }
                        }
                print(measure)
                #temp = "{0:.2f}".format(temp)
                data.append(measure)
                (rc, mid) = mqttc.publish("onewire/temperature" + sensor['id'], json.dumps(measure), qos=0)
            except:
                temp = ''
        #print(data)
        client.write_points(data)
        time.sleep(300)


if __name__ == '__main__':
    main()


